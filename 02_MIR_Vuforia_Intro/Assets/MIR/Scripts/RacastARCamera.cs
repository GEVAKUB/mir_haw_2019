﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacastARCamera : MonoBehaviour {

    private RaycastHit hit;
    private bool hitObject = false;
    private GameObject spinning;
    private SpinningCube sc;

	// Use this for initialization
	void Start () {
		if (this.gameObject.name.Equals("Cube"))
        {
            spinning = GameObject.Find("Spinning");
            sc = spinning.GetComponent<SpinningCube>();
            spinning.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
        Transform cam = Camera.main.transform;
        Ray ray = new Ray(cam.position, cam.forward);
        Debug.DrawRay(ray.origin, ray.direction * 1000.0f, Color.green, 10, false);

        if (Physics.Raycast (ray, out hit, 1000.0f))
        {
            if (hit.transform.name == this.transform.name)
            {
                hitObject = true;
                if (this.gameObject.name.Equals("Cube"))
                    sc.speed = (Camera.main.transform.position - 
                        this.transform.position).magnitude * 2000.0f;
            }
            else
            {
                hitObject = false;
            }

            if (hitObject)
            {
                ChangeMaterialColor(this.transform.gameObject, Color.green);
                if (this.gameObject.name.Equals("Cube"))
                {
                    
                    spinning.SetActive(true);
                }
            }
            else
            {
                ChangeMaterialColor(this.transform.gameObject, Color.blue);
                if (this.gameObject.name.Equals("Cube"))
                {
                    
                    spinning.SetActive(false);
                }
            }
        }
	}

    private void ChangeMaterialColor (GameObject target, Color matColor)
    {
        Material material = new Material(Shader.Find("Transparent/Diffuse"));
        material.color = matColor;
        target.GetComponent<Renderer>().material = material;
    }
}
