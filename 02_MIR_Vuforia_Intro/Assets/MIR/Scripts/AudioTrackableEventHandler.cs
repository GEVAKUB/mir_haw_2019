﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class AudioTrackableEventHandler : MonoBehaviour, ITrackableEventHandler {

    private TrackableBehaviour mTrackableBehaviour;
    private AudioSource audioSource;
    private AudioLowPassFilter alpf;

	// Use this for initialization
	void Start () {
        mTrackableBehaviour = this.GetComponent<TrackableBehaviour>();
        audioSource = this.GetComponent<AudioSource>();
        alpf = this.GetComponent<AudioLowPassFilter>();

        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        } 

	}
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = Camera.main.transform.position;
        Vector3 delta = pos - this.transform.position;
        float distance = delta.magnitude;
        alpf.cutoffFrequency = distance * 5000.0f;
	}

    void ITrackableEventHandler.OnTrackableStateChanged (TrackableBehaviour.Status prevStatus,
                                                            TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            audioSource.Play();
        }
        else if (prevStatus == TrackableBehaviour.Status.TRACKED &&
                newStatus == TrackableBehaviour.Status.NOT_FOUND)
        {
            audioSource.Stop();
        }
    }


}
