﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReceiver : MonoBehaviour {

    private Server server;
    public GameObject serverGameObject;
    public GameObject receiver;
	// Use this for initialization
	void Start () {
        server = serverGameObject.GetComponent<Server>();
	}
	
	// Update is called once per frame
	void Update () {
        receiver.transform.position = server.acc;
        receiver.transform.rotation = server.attitude;

	}
}
